#!/usr/bin/env python3

import argparse
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import itertools
import pandas as pd
import os
import subprocess
import json
import sys

class VTuneTablePlotter:
    
    __LOGNAME_LABEL: str = 'Label'
    __RED_FLAG_LABEL: str = 'Red flag zone'
    __ELAPSED_TIME_LABEL: str = 'Elapsed Time'

    __PDF_TKN: str = 'pdf'
    __SVG_TKN: str = 'svg'
    __PNG_TKN: str = 'png'

    __SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
    with open(os.path.join(__SCRIPT_DIR, 'vtune_red_flag_thresholds.json')) as __f:
        __RED_FLAGS_ZONES = json.load(__f)

    def plot(args: argparse.Namespace):
        
        HATCHES = itertools.cycle(('//', '\\\\', '||', '--', '++', 'xx', 'oo', '..', '**', 'OO'))
        MARKERS = itertools.cycle(('s', 'v', '^', '<', '>', 'p', 'D', 'X', '*'))

        METRICS = ['Retiring', 'Bad Speculation', 'Front-End Bound', 'Core Bound', 'Memory Bound']
        COLORS = itertools.cycle((mpl.colormaps['Set2'].colors))
        METRIC_COLORS = [next(COLORS) for _ in METRICS]

        RESULTS_DIR = args.dir
        FILENAME = args.table

        df = pd.read_csv(os.path.join(RESULTS_DIR, FILENAME + '.csv'))

        title = df.columns.values[0]

        new_col_order = df.columns.tolist()
        new_col_order[1:] = df.drop(columns=df.columns[0]).sort_values(df.index[df[title] == VTuneTablePlotter.__LOGNAME_LABEL].tolist()[0], axis=1).columns.tolist()
        df = df[new_col_order]

        apps = df.columns.values[1:]
        DATA = {}
        for i in df.index:
            row = df.iloc[i].values.flatten()
            label = row[0]
            reader = lambda item: float(item[0])
            if label == VTuneTablePlotter.__ELAPSED_TIME_LABEL:
                reader = lambda item: float(item[0])
            elif label == VTuneTablePlotter.__LOGNAME_LABEL:
                reader = lambda item: str(item[0]).replace(' / ', '\n') + (' - [{}]'.format(item[1]) if args.vtlog else '')
            DATA[label] = list(map(reader, zip(row[1:], apps)))

        fig, ax = plt.subplots(1, 1, sharey=True, figsize=(12, max(len(apps) * 0.75, 2)))
        HEIGHT = 0.5
        LOG_NAMES = DATA[VTuneTablePlotter.__LOGNAME_LABEL]

        X = np.arange(len(LOG_NAMES))
        left = np.zeros(len(LOG_NAMES))
        for metric, color in zip(METRICS, METRIC_COLORS):
            values = np.clip(np.array(DATA[metric]), 0, 100 - left)
            if metric == METRICS[-1]:
                values = 100 - left
            ax.barh(X, values, height=HEIGHT, left=left, color=color, label=metric, zorder=2)
            for x, value, left_value in zip(X, values, left):
                # Print value in the bar
                ax.text(value / 2 + left_value, x, '{:2.1f}'.format(value), horizontalalignment='center', verticalalignment='center', fontsize=8)
                # Draw red flag zones
                if metric in VTuneTablePlotter.__RED_FLAGS_ZONES:
                    thres = VTuneTablePlotter.__RED_FLAGS_ZONES[metric]
                    if value >= thres:
                        xx = x - 0.25
                        ax.plot([left_value, left_value + value - thres], [xx, xx], zorder=4, color='#971a25', marker='|', lw=2, markersize=10)
            left += values

        # ------------------------ Add time values on the side ----------------------- #
        TIME_X = 106
        time_color = next(COLORS)
        time_color = next(COLORS)
        data_time = DATA[VTuneTablePlotter.__ELAPSED_TIME_LABEL]
        mini, maxi = min(data_time), max(data_time)
        if mini == maxi:
            maxi = mini + 1
        for x, value in zip(X, data_time):
            t = ax.text(TIME_X, x, '{:.1f}'.format(value).rjust(8), horizontalalignment='center', verticalalignment='center', family='monospace')
            t.set_bbox(dict(facecolor=time_color, alpha=0.5 + (value - mini) / (maxi - mini) * 0.5, edgecolor='none', boxstyle='square,pad=0.45'))

        ax.plot(-1, 0, zorder=4, color='#971a25', marker='|', lw=2, markersize=10, label=VTuneTablePlotter.__RED_FLAG_LABEL) # Outside of plot, for label only

        ax.set_yticks(np.arange(len(LOG_NAMES)))
        ax.set_yticklabels(LOG_NAMES)
        ax.set_xlim(0, 100)
        ax.set_ylim(-0.5, len(LOG_NAMES) + 0.5)
        ax.set_xticks([50, TIME_X])
        ax.set_xticklabels(['% of Pipeline Slots', 'Time (s)'])
        ax.spines[['right', 'top', 'left', 'bottom']].set_visible(False)

        ax.tick_params(axis=u'both', which=u'both',length=0)
        handles, labels = ax.get_legend_handles_labels()
        order = [1, 2, 3, 4, 5, 0]
        ax.legend([handles[idx] for idx in order],[labels[idx] for idx in order], loc='upper left', frameon=False, ncol=6)

        if args.print:
            print(df)

        plt.title(title, style='italic')
        plt.tight_layout()

        # -------------------------------- Maybe show -------------------------------- #
        if not args.no_view:
            plt.show()

        # ------------------------------ Save the figure ----------------------------- #
        if args.save != None:
            if VTuneTablePlotter.__SVG_TKN in args.save or VTuneTablePlotter.__PDF_TKN in args.save:
                svg_path = os.path.join(RESULTS_DIR, FILENAME + '.svg')
                fig.savefig(svg_path)
                if VTuneTablePlotter.__PDF_TKN in args.save:
                    subprocess.run(['inkscape -D {} -o {}'.format(svg_path, svg_path.replace('.svg', '.pdf'))], shell=True)
                if not VTuneTablePlotter.__SVG_TKN in args.save:
                    os.remove(svg_path)
            if VTuneTablePlotter.__PNG_TKN in args.save:
                fig.savefig(os.path.join(RESULTS_DIR, FILENAME + '.png'))
    
    def execute(args):
        parser = argparse.ArgumentParser(description='Plot VTune results from a csv table')
        parser.add_argument('dir', type=str, help='Input and output directory')
        parser.add_argument('table', type=str, help='Table name (without extension)')
        parser.add_argument('--no-view', dest='no_view', action='store_true', help='Do not show the plot with the matplotlib viewer')
        parser.add_argument('--save', choices=[
            VTuneTablePlotter.__PDF_TKN,
            VTuneTablePlotter.__SVG_TKN,
            VTuneTablePlotter.__PNG_TKN], nargs='*', help='Save the plot in specified format')
        parser.add_argument('--vtlog', action='store_true', help='Write VTune log names in the plot labels')
        parser.add_argument('--print', action='store_true', help='Print the data table')
        VTuneTablePlotter.plot(parser.parse_args(args))

if __name__ == '__main__':
    VTuneTablePlotter.execute(sys.argv[1:])