#!/usr/bin/env python3

import os
import sys
import subprocess
import shutil
import pandas as pd
from enum import Enum
from typing_extensions import Self
from treelib import Tree
import matplotlib.pyplot as plt
import numpy as np
import json
import logging

from .vtune_table_plot import VTuneTablePlotter

class VTuneResultTable:

    __CSV_SEP = ','

    __LABEL_TOKEN = 'Label'
    __RESULTS_TYPES = [
        __LABEL_TOKEN, 'Retiring', 'Bad Speculation', 'Front-End Bound', 'Back-End Bound',
        'Core Bound', 'Memory Bound', 'Elapsed Time', 'IPC Rate', 'Memory Bandwidth', 'Memory Latency',
    ]

    name: str
    save_dir: str
    save_path: str

    table: pd.DataFrame

    def __init__(self, name: str, save_dir: str) -> None:
        self.name = name
        self.save_dir = save_dir
        self.save_path = os.path.join(save_dir, name + '.csv')
        if os.path.exists(self.save_path):
            self.__load()
        else:
            self.__create()
            self.__save()
    
    def get_save_path(self) -> str:
        return self.save_path

    def print(self) -> Self:
        print(self.table)
        return self

    def __save(self) -> None:
        if not os.path.exists(self.save_dir):
            os.mkdir(self.save_dir)
        self.table.to_csv(self.save_path, sep=self.__CSV_SEP, index=False)
    
    def __load(self):
        self.table = pd.read_csv(self.save_path, sep=self.__CSV_SEP)
    
    def __create(self, title: str = 'Untitled'):
        self.table = pd.DataFrame(data={title: self.__RESULTS_TYPES})

    def set_title(self, title: str) -> Self:
        self.table = self.table.rename({self.table.columns[0]: title}, axis='columns')
        self.__save()
        return self
    
    def __retrieve_data_result(self, type, dict_report: dict):
        if type == 'IPC Rate':
            return str(1.0 / float(dict_report['CPI Rate']))
        elif type == 'Elapsed Time':
            return str(float(dict_report['Elapsed Time']) - float(dict_report['Paused Time']))
        else:
            return dict_report[type]

    def __add_data(self, log_name: str, data: list) -> None:
        if len(data) > 0:
            self.table[log_name] = data
            self.__save()
    
    def __get_data(self, log_name: str) -> list:
        return self.table[log_name]

    def add_data(self, log_name: str, label: str, raw_report: str) -> Self:
        data = []
        dict_report = {}
        for line in raw_report.split('\n'):
            row = line.split(',')
            if len(row) > 2:
                dict_report[row[1]] = row[2]
        for type in self.__RESULTS_TYPES:
            if type == self.__LABEL_TOKEN:
                data.append(label)
            else:
                try:
                    data.append(self.__retrieve_data_result(type, dict_report))
                except KeyError:
                    print('Error: report is probably empty')
                    return
        self.__add_data(log_name, data)
        return

    def set_label(self, log_name: str, label: str) -> Self:
        if log_name in self.table.columns:
            self.table.at[0, log_name] = label
            self.__save()
        return self

    def remove_data(self, log_name: str) -> Self:
        if log_name in self.table.columns:
            self.table = self.table.drop(log_name, axis=1)
            self.__save()
        return self
    
    def reset(self) -> Self:
        title = self.table.columns[0]
        self.__create(title)
        return self
    
    def get_log_labels(self) -> iter:
        return zip(self.table.columns.values[1:], self.table.loc[self.table[self.table.columns[0]] == self.__LABEL_TOKEN].values.flatten().tolist()[1:])
    
    def get_logs(self) -> iter:
        return self.table.columns.values[1:]
    
    def append(self, other_table: Self) -> None:
        iterator = list(other_table.get_logs())
        for log_name in iterator:
            self.__add_data(log_name, other_table.__get_data(log_name))
    
    def plot(self, save_pdf: bool = False, save_png: bool = False, save_svg: bool = False, no_view: bool = False) -> None:
        args = [self.save_dir, self.name]
        if save_pdf or save_png or save_svg:
            args.append('--save')
        if save_pdf:
             args.append('pdf')
        if save_png:
            args.append('png')
        if save_svg:
            args.append('svg')
        if no_view:
            args.append('--no-view')
        VTuneTablePlotter.execute(args)


class VTuneWrapper:

    class AnalysisType(str, Enum):
        MICRO_ARCH = 'uarch-exploration'
        MEMORY = 'memory-access'
    
    __VTUNE_DEFAULT_DIR = '/opt/intel/oneapi/vtune/latest'
    vtune_dir: str

    __PREPARE_COMMAND_RAW = (
        '. {0}/vtune-vars.sh'
        + ' && export INTEL_LIBITTNOTIFY32={0}/lib32/runtime/libittnotify_collector.so'
        + ' && export INTEL_LIBITTNOTIFY64={0}/lib64/runtime/libittnotify_collector.so'
    )
    __prepare_command: str

    __DEFAULT_RUN_CONFIG = {
        'analysis_type': AnalysisType.MICRO_ARCH.value,
        'result_dir': '/tmp/vtune-results',
        'summary_only': False,
        'start_paused': False,
        'program': None,
        'start_after': 0,
        'stop_after': 0,
    }
    run_config: dict

    _logger: logging.Logger

    def __init__(self, vtune_dir: str = __VTUNE_DEFAULT_DIR) -> None:
        if not os.path.exists(os.path.join(vtune_dir, 'vtune-vars.sh')):
            self._logger.error('Could not find VTune at {}'.format(vtune_dir))
            raise ValueError()
        self.vtune_dir = vtune_dir
        self.__prepare_command = self.__PREPARE_COMMAND_RAW.format(vtune_dir)
        self.run_config = self.__DEFAULT_RUN_CONFIG

        self._logger = logging.getLogger(self.__class__.__name__)
        if (self._logger.hasHandlers()):
            self._logger.handlers.clear()
        self._logger.setLevel(logging.INFO)
        handler = logging.StreamHandler(stream=sys.stdout)
        formatter = logging.Formatter('[%(asctime)s - %(name)s - %(levelname)s] %(message)s', '%Y-%m-%d %H:%M:%S')
        handler.setFormatter(formatter)
        self._logger.addHandler(handler)

    def set_run_analysis_type(self, analysis_type: AnalysisType) -> None:
        self.run_config['analysis_type'] = analysis_type.value
    
    def set_run_summary_only(self, active: bool) -> None:
        self.run_config['summary_only'] = active
        if active:
            self._logger.warning('ITT API calls may not work properly when using VTune summary mode')
    
    def set_run_start_paused(self, active: bool) -> None:
        self.run_config['start_paused'] = active

    def set_run_result_dir(self, result_dir: str) -> None:
        self.run_config['result_dir'] = result_dir
    
    def set_run_start_after(self, value: int = 0) -> None:
        self.run_config['start_after'] = value
    
    def set_run_stop_after(self, value: int = 0) -> None:
        self.run_config['stop_after'] = value
    
    def set_run_program(self, program: str) -> None:
        if os.path.exists(program):
            if os.access(program, os.X_OK):
                 self.run_config['program'] = program
            else:
                self._logger.error('Program {} is not executable'.format(program))
        else:
            self._logger.error('Program {} does not exists'.format(program))

    def run(self, log_name: str, args: str, can_overwrite_results: bool = False) -> bool:
        if self.run_config['program'] == None:
            self._logger.error('No program. Call set_program first')
        if not os.path.exists(self.run_config['result_dir']):
            os.makedirs(self.run_config['result_dir'])
        result_path = os.path.join(self.run_config['result_dir'], log_name)
        if os.path.exists(result_path):
            self._logger.info('Result path {} already exists'.format(result_path))
            if can_overwrite_results:
                self._logger.info('Can overwrite: will delete old dir')
                shutil.rmtree(result_path)
            else:
                self._logger.info('Cannot overwrite: doing nothing')
                return False
        
        # ------------------------- Add more args with config ------------------------ #
        more_args = ''
        if self.run_config['start_paused']:
            more_args += '--start-paused '
        if self.run_config['summary_only']:
            more_args += '-knob pmu-collection-mode=summary '
        if self.run_config['start_after'] > 0:
            more_args += '--resume-after {} '.format(self.run_config['start_after'])
        if self.run_config['stop_after'] > 0:
            more_args += '--duration {} '.format(self.run_config['stop_after'])
        
        final_run_config = {'result_path': result_path, 'args': args, **self.run_config, 'more_args': more_args}
        exec_command = 'vtune -collect {analysis_type} {more_args} -quiet -no-summary -result-dir={result_path} -- {program} {args}'.format(**final_run_config)
        command = self.__prepare_command+ ' && ' + exec_command
        self._logger.info(exec_command)
        subprocess.run([command], shell=True)
        return True

    def add_results_to_table(self, log_name: str, label: str, table: VTuneResultTable):
        result_path = os.path.join(self.run_config['result_dir'], log_name)
        command = (
            self.__prepare_command
            + ' && vtune -report summary -r {0} -format csv -csv-delimiter comma'.format(result_path)
        )
        result = subprocess.run(
            [command], capture_output=True, text=True, shell=True)
        if result.stdout == '':
            self._logger.error('Could not read report')
            return
        table.add_data(log_name, label, result.stdout)

    def run_and_add_results_to_table(self, log_name: str, args: str, label: str, table: VTuneResultTable, can_overwrite_results: bool = False) -> None:
        if self.run(log_name, args, can_overwrite_results):
            self.add_results_to_table(log_name, label, table)

    def regen_table(self, table: VTuneResultTable) -> VTuneResultTable:
        iterator = list(table.get_log_labels())
        table.reset()
        for log_name, label in iterator:
            self._logger.info('Regen {} [{}]'.format(label, log_name))
            self.add_results_to_table(log_name, label, table)
        return table

    __SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
    with open(os.path.join(__SCRIPT_DIR, 'vtune_red_flag_thresholds.json')) as __f:
        __RED_FLAGS_ZONES = json.load(__f)

    __MAIN_METRICS = ['Retiring', 'Bad Speculation', 'Front-End Bound', 'Back-End Bound', 'Core Bound', 'Memory Bound']

    def print_red_flagged_metrics(self, table: VTuneResultTable, labels: list = [], show_all_metrics: bool = False) -> None:
        save_path = table.get_save_path().rstrip('.csv')
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        
        iterator = list(table.get_log_labels())
        for log_name, label in iterator:
            
            if len(labels) != 0 and label not in labels:
                continue
                
            result_path = os.path.join(self.run_config['result_dir'], log_name)
            command = (
                self.__prepare_command
                + ' && vtune -report summary -r {0} -format csv -csv-delimiter comma -report-knob show-issues=true'.format(result_path)
            )
            result = subprocess.run(
                [command], capture_output=True, text=True, shell=True)
            
            tree = Tree()
            idx = 0
            current_parents = {}
            tree.create_node(label, idx)
            current_parents[0] = idx
            idx += 1
            for line in result.stdout.split('\n'):
                row = line.split(',')
                if len(row) < 2:
                    continue
                label = row[1]
                if len(row) > 1:
                    if label == 'Average CPU Frequency':
                        break # No need to read further
                    if label == 'CPI Rate':
                        continue
                if label in self.__MAIN_METRICS or (len(row) > 3 and row[3].startswith('0x') and (int(row[3], 16) > 0 or show_all_metrics)):
                    level = int(row[0])
                    if level > 0:
                        if level == 1 and label not in self.__MAIN_METRICS:
                            continue
                        value = float(row[2]) if row[2] != '' else 0
                        thres = self.__RED_FLAGS_ZONES[label] if label in self.__RED_FLAGS_ZONES else -1
                        if value >= thres or label in self.__MAIN_METRICS or show_all_metrics:
                            parent_level = level - 1
                            while parent_level not in current_parents:
                                parent_level -= 1
                            tree.create_node('{},{},{},{}'.format(label, value, thres, level), idx, current_parents[parent_level])
                            current_parents[level] = idx
                            idx += 1
            
            x = []
            y_value = []
            y_thres = []
            y_types = []
            max_x_size = 0
            title = ''
            for line in str(tree).split('\n'):
                data = line.split(',')
                if len(data) > 1:
                    x.append(data[0])
                    max_x_size = max(max_x_size, len(data[0]))
                    y_value.append(float(data[1]))
                    y_thres.append(float(data[2]))
                    y_types.append('C' if float(data[3]) > 2 else 'P')
                elif title == '':
                    title = data[0]
                    max_x_size = max(max_x_size, len(data[0]))
            
            if len(x) == 0:
                continue

            max_x_size = 48 # Hardcoded

            pad = lambda e, c: (e + ' ').ljust(max_x_size, c)
            x = [pad(e, '─') for e in x]
            title = pad(title, ' ')

            x.reverse()
            y_value.reverse()
            y_thres.reverse()
            y_types.reverse()

            x = np.array(x)
            y_value = np.array(y_value)
            y_thres = np.array(y_thres)
            y_types = np.array(y_types)

            height = 0.5

            fig = plt.figure(figsize=(16, len(x) * 0.25))
            ax = fig.add_subplot(111)

            RED_FLAG_COLOR = '#971a25'
            OK_COLOR = '#022f8e'
            
            X = np.arange(len(x)) - 0.1

            ax.barh(X, y_thres, align='center', height=height, color='#A7C7E7', label='Better zone', zorder=3)
            ax.barh(X, y_value, height=height, color='none', edgecolor=OK_COLOR, linewidth=1, zorder=4)
            data = y_value - y_thres
            mask = np.where(data >= 0.0)
            ax.barh(X[mask], data[mask], left=y_thres[mask], height=height, color='none', edgecolor=RED_FLAG_COLOR, linewidth=1, hatch='/////', label='Red flag zone', zorder=4)
            ax.barh(X, 100, height=height, color='#ffbe61', label='Worse zone', zorder=2)

            for a, b in zip(np.arange(len(x)), y_types):
                ax.text(101, a - height * 3 / 4, b, fontname='monospace')
            
            ax.set_yticks(np.arange(len(x) + 1))
            ax.set_yticklabels(list(x) + [title], fontname='monospace', ha='right')
            ylabel_colors = list(np.vectorize(lambda x: RED_FLAG_COLOR if x >= 0 else OK_COLOR)(data).flatten()) + ['black']
            for t, color in zip(ax.get_yticklabels(), ylabel_colors):
                t.set_color(color)
            ax.set_xticks(np.arange(0, 101, 10))
            ax.set_xticklabels(list(map(lambda x: '{:.0f}%'.format(x), ax.get_xticks())))
            ax.tick_params(left=False)
            ax.spines[['right', 'top', 'left']].set_visible(False)
            ax.set_xlim([-1, 101])
            ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.0 + 1.2 / len(x)), frameon=False, ncol=4)
        
            plt.grid(True, axis='x')
            plt.tight_layout()
            plt.show()
            
            fig.savefig(os.path.join(save_path, log_name + '.svg'), bbox_inches='tight')
            fig.savefig(os.path.join(save_path, log_name + '.png'), bbox_inches='tight')
